//
//  O2TableClass.m
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "O2TableClass.h"

@implementation O2TableClass

#pragma mark -

- (O2TableClass *) init {
    return [self initNormal];
//    return [self init1Phase];
//    return [self init2Phases];
//    return [self init3Phases];
//    return [self init4Phases];
}

-(O2TableClass*) initNormal {
    return [self initWithTimeBase:10];
}

-(O2TableClass*) initWithTimeBase:(int) baseTime {
    NSArray *phases = @[
                        NSLocalizedString(@"ventilate", nil),
                        NSLocalizedString(@"hold", nil)
                        ];
    
    NSArray *abbr   = @[
                        NSLocalizedString(@"ventilate", nil),
                        NSLocalizedString(@"hold", nil)
                        ];
    
    NSArray *steps   =  @[
                          @[@(baseTime*12), @(baseTime*12)], //120 x 120
                          @[@(baseTime*12), @(baseTime*14)], //120 x 135
                          @[@(baseTime*12), @(baseTime*16)], //120 x 150
                          @[@(baseTime*12), @(baseTime*18)], //120 x 165
                          @[@(baseTime*12), @(baseTime*18)], //120 x 180
                          @[@(baseTime*12), @(baseTime*19)], //120 x 190
                          @[@(baseTime*12), @(baseTime*19)], //120 x 190
                          @[@(baseTime*12), @(baseTime*19)]  //120 x 190
                          ];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps andTableName:@"O2" andTimeBase:baseTime];
    return self;
}

- (void) reinit:(int) baseTime; {
    NSArray *steps  = @[
                        @[@(baseTime*12), @(baseTime*12)], // 120 x 120
                        @[@(baseTime*12), @((int)((float)baseTime*13.5))], // 120 x 135
                        @[@(baseTime*12), @(baseTime*15)], // 120 x 150
                        @[@(baseTime*12), @((int)((float)baseTime*16.5))], // 120 x 165
                        @[@(baseTime*12), @(baseTime*18)], // 120 x 180
                        @[@(baseTime*12), @(baseTime*19)], // 120 x 190
                        @[@(baseTime*12), @(baseTime*19)], // 120 x 190
                        @[@(baseTime*12), @(baseTime*19)]  // 120 x 190
                        ];
    self.stepsInitial = (NSMutableArray*)steps;
    self.timeBase     = baseTime;
    [self reset];
}

#pragma => Testing Tables

-(O2TableClass*) init4Phases {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"full lungs apnea", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"empty lungs apnea", nil)
                       ];
    NSArray *abbr   = @[
                        @"IN",
                        @"APFL",
                        @"EX",
                        @"APEL"];
    NSArray *steps  = @[
                        @[@5.0, @5.0, @10, @0],
                        @[@5.0, @5.0, @10, @5],
                       ];
    //    NSArray *cycles = @[@1, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"O2" andTimeBase:10];
    return self;
}

-(O2TableClass*) init3Phases {
    NSArray *phases = @[@"inhale", @"full lungs apnea", @"exhale"];
    NSArray *abbr   = @[@"IN", @"APFL", @"EX"];
    NSArray *steps  = @[
                        @[@1.0, @3.0, @2],
                        @[@2.0, @1.0, @1],
                        @[@3.0, @2.0, @3]
                       ];
    //    NSArray *cycles = @[@2, @2, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"O2" andTimeBase:10];
    return self;
}

-(O2TableClass*) init2Phases {
    NSArray *phases = @[@"inhale", @"exhale"];
    NSArray *abbr   = @[@"inhale", @"exhale"];
    NSArray *steps  = @[
                        @[@5.0, @3.0],
                        @[@5.0, @4.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0],
                        @[@5.0, @5.0]
                       ];
    //    NSArray *cycles = @[@2, @2, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"O2" andTimeBase:10];
    return self;
}

-(O2TableClass*) init1Phase {
    NSArray *phases = @[@"inhale"];
    NSArray *abbr   = @[@"inhale"];
    NSArray *steps  = @[
                        @[@5.0],
                        @[@5.0],
                        @[@5.0]
                       ];
    //    NSArray *cycles = @[@2, @2, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"O2" andTimeBase:10];
    return self;
}

@end
