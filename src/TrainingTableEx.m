//
//  TrainingTableEx.m
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

/**
 **   Extension of TrainingTable class with added support for
 **   number of cycles for each training step
 **/

#import "Tools.h"

#import "TrainingTableEx.h"

@implementation TrainingTableEx

#pragma mark - Initialization

- (TrainingTableEx*) initWithPhases:(NSArray*) phases_ andPhasesAbbr:(NSArray*) phasesAbbr_ andSteps:(NSArray*) steps_ andCycles:(NSArray*) cycles_ andTableName:(NSString*) tableName_ andTimeBase:(int)timeBase_{
    self = [super initWithPhases:phases_ andPhasesAbbr:phasesAbbr_ andSteps:steps_ andTableName:tableName_ andTimeBase:timeBase_];
    self.cyclesInitial = cycles_;
    [self reset];
    return self;
}

- (TrainingTableEx*) initWithPhases:(NSArray*) phases_ andPhasesAbbr:(NSArray*) phasesAbbr_ andSteps:(NSArray*) steps_ andTableName:(NSString*) tableName_ andTimeBase:(int)timeBase_ {
    self = [super initWithPhases:phases_ andPhasesAbbr:phasesAbbr_ andSteps:steps_ andTableName:tableName_ andTimeBase:timeBase_];
    self.cyclesInitial = [self setOneCyclePerStep];
    [self reset];
    return self;
}

- (NSArray*) setOneCyclePerStep {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.steps.count; i++) {
        [result addObject:[NSNumber numberWithInt:1]];
    }
    return result;
}

- (void) reset {
    if (self.cyclesInitial != nil)
        self.cycles = [[NSMutableArray alloc] initWithArray:self.cyclesInitial copyItems:YES];
    [super reset];
    self.totalTime = [self getTotalTime];
}

- (void) reinitWithRepeats:(int)repeats {
    mustOverride(); // or methodNotImplemented(), same thing
}

- (void) reinitStepPhases {
    NSArray *ia = self.stepsInitial[self.currentStep];
    NSMutableArray *wa = [[NSMutableArray alloc] initWithArray:ia copyItems:YES];
    [self.steps replaceObjectAtIndex:self.currentStep withObject:wa];
}

#pragma mark - Utility Functions

- (void) printArrays:(BOOL) withInitial {
    if (withInitial) {
        printf("Initial Array:\n");
        for (int i = 0; i < self.stepsInitial.count; i++) {
            NSArray *arr = self.stepsInitial[i];
            for (NSNumber *val in arr) {
                printf("%.2f  ", val.floatValue);
            }
            printf("  (x%d)\n", [self.cyclesInitial[i] intValue]);
        }
    }
    printf("Working Array:\n");
    for (int i = 0; i < self.steps.count; i++) {
        NSArray *arr = self.steps[i];
        for (NSNumber *val in arr) {
            printf("%.2f  ", val.floatValue);
        }
        if (self.cycles != nil)
            printf("  (x%d)\n", [self.cycles[i] intValue]);
        else
            printf("\n");
    }
}

- (float) getTotalTime {
    if (self.cyclesInitial == nil &&
        self.cycles == nil) {
        return 0;
    } else {
        float result = 0;
        for (int i = 0; i < self.stepsInitial.count; i++) {
            float stepTime = 0;
            //if ([self.cycles[i] intValue] > 0) {
            for (NSNumber * val in self.stepsInitial[i]) {
                stepTime += [val floatValue];
            }
            result += stepTime * ([self.cycles[i] floatValue]);
            //}
            if ([self.cycles[i] intValue] != [self.cyclesInitial[i] intValue]) {
                for (NSNumber *val in self.steps[i]) {
                    result += [val floatValue];
                }
            }
        }
//        NSLog(@"s: %d  p: %d  cycles[s]: %d  TT: %.0f", self.currentStep, self.currentPhase, [self.cycles[self.currentStep] intValue], result);
        return result;
    }
}

-(float) calculateTotalTime {
    return 0;
}

#pragma mark - Training Table Operations

 /*************************************************************
  *
  *  !!!! due to code architechture limitations first 
  *       phase of TrainingTableEx CAN NOT have zero-duration
  *
  *************************************************************/
- (int) nextPhase:(BOOL) consumeCycle {
    [super nextPhase:consumeCycle];
    if (consumeCycle && self.currentPhase == 0) {
        [self.cycles replaceObjectAtIndex:self.currentStep
                               withObject:[NSNumber numberWithInt:[self.cycles[self.currentStep] intValue] - 1]];
    }
    self.totalTime = [self getTotalTime];
    return self.currentPhase;
}

- (int) nextStep {
    if ([self.cycles[self.currentStep] intValue] > 0) {
//        self.totalTime = [self getTotalTime];
        self.currentPhase = 0;
        [self reinitStepPhases];
        return self.currentStep;
    } else {
        return [super nextStep];
    }
}

- (int) getNextStep {
    if ([self.cycles[self.currentStep] intValue] > 0)
        return self.currentStep;
    else
        return [super getNextStep];
}

- (PhaseId) getNextPhaseIdForStep:(int)step andPhase:(int)phase {
    PhaseId pi;
    pi.step  = step;
    pi.phase = phase + 1;
    if (pi.phase >= self.phases.count) {
        pi.phase = 0;
        if ([self.cycles[step] intValue] == 0)
            pi.step++;
    }
    if ([self getInitialPhaseDuration:pi.phase forStep:pi.step] == 0) {
        return [self getNextPhaseIdForStep:pi.step andPhase:pi.phase];
    } else {
        return pi;
    }
}

@end
