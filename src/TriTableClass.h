//
//  TriTableClass.h
//  Apnea Timer
//
//  Created by Mikhail on 16.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableEx.h"

@interface TriTableClass : TrainingTableEx

-(TriTableClass*) initTableA:(NSString*)tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_;
-(TriTableClass*) initTableB:(NSString*)tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_;

@end
