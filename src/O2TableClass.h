//
//  O2TableClass.h
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTable.h"
#import "TrainingTableEx.h"

@interface O2TableClass : TrainingTable
-(O2TableClass*) initWithTimeBase:(int) baseTime;
@end
