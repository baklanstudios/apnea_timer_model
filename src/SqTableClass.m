//
//  SqTableClass.m
//  Apnea Timer
//
//  Created by Mikhail on 16.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "SqTableClass.h"

@implementation SqTableClass

-(SqTableClass*) initTableA:(NSString*) tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_ {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"full lungs apnea", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"empty lungs apnea", nil)
                       ];
    
#ifdef BUILD_FOR_IPAD
    NSArray *abbr   = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"hold", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"hold", nil)
                        ];
#else
    NSArray *abbr   = @[
                        NSLocalizedString(@"IN", nil),
                        NSLocalizedString(@"APFL", nil),
                        NSLocalizedString(@"EX", nil),
                        NSLocalizedString(@"APEL", nil)
                       ];
#endif
    
    int n = timeBase_;
    NSArray *steps   =  @[
                          @[@(n), @0,     @(2*n), @0],
                          @[@(n), @(n),   @(2*n), @0],
                          @[@(n), @(n),   @(2*n), @(n)],
                          @[@(n), @(2*n), @(2*n), @(n)],
                          @[@(n), @(2*n), @(2*n), @(2*n)],
                          ];
    
    NSArray *cycles = @[@(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_)]; //@8
    
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps andCycles:cycles andTableName:tableName_ andTimeBase:timeBase_];
    
    self.repeats = repeats_;

    return self;
}

-(SqTableClass*) initTableB:(NSString*) tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_ {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"full lungs apnea", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"empty lungs apnea", nil)
                        ];
#ifdef BUILD_FOR_IPAD
    NSArray *abbr   = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"hold", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"hold", nil)
                        ];
#else
    NSArray *abbr   = @[
                        NSLocalizedString(@"IN", nil),
                        NSLocalizedString(@"APFL", nil),
                        NSLocalizedString(@"EX", nil),
                        NSLocalizedString(@"APEL", nil)
                        ];
#endif
    int n = timeBase_;
    NSArray *steps   =  @[
                          @[@(n)  , @(n)  , @(n*2)    , @(n  )],
                          @[@(n+1), @(n+1), @((n+1)*2), @(n+1)],
                          @[@(n+2), @(n+2), @((n+2)*2), @(n+2)],
                          @[@(n+3), @(n+3), @((n+3)*2), @(n+3)],
                          @[@(n+4), @(n+4), @((n+4)*2), @(n+4)],
                          ];
    
    NSArray *cycles = @[@(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_)]; //@6
    
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps andCycles:cycles andTableName:tableName_ andTimeBase:timeBase_];
    
    self.repeats = repeats_;
    
    return self;
}

-(SqTableClass*) initTableC:(NSString*) tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_ {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"full lungs apnea", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"empty lungs apnea", nil)
                        ];

#ifdef BUILD_FOR_IPAD
    NSArray *abbr   = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"hold", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"hold", nil)
                        ];
#else
    NSArray *abbr   = @[
                        NSLocalizedString(@"IN", nil),
                        NSLocalizedString(@"APFL", nil),
                        NSLocalizedString(@"EX", nil),
                        NSLocalizedString(@"APEL", nil)
                        ];
#endif
    int n = timeBase_;
    NSArray *steps   =  @[
                          @[@(n), @((n+0)*2), @(n*2), @(n)],
                          @[@(n), @((n+1)*2), @(n*2), @(n+1)],
                          @[@(n), @((n+2)*2), @(n*2), @(n+2)],
                          @[@(n), @((n+3)*2), @(n*2), @(n+3)],
                          @[@(n), @((n+4)*2), @(n*2), @(n+4)],
                          ];
    
    NSArray *cycles = @[@(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_)]; //@6
//    NSArray *cycles = @[@6, @6, @6, @6, @6]; //@6
    
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps andCycles:cycles andTableName:tableName_ andTimeBase:timeBase_];

    self.repeats = repeats_;
    
    return self;
}

- (void) reinitWithRepeats:(int) repeats; {
    NSArray *cycles;
    if ([self.tableName isEqualToString:@"SqrA"]) {
        cycles = @[@(repeats), @(repeats), @(repeats), @(repeats), @(repeats)];
    } else if ([self.tableName isEqualToString:@"SqrB"]) {
        cycles = @[@(repeats), @(repeats), @(repeats), @(repeats), @(repeats)];
    } else if ([self.tableName isEqualToString:@"SqrC"]) {
        cycles = @[@(repeats), @(repeats), @(repeats), @(repeats), @(repeats)];
    }
    self.cyclesInitial = (NSMutableArray*)cycles;
    self.repeats       = repeats;
    [self reset];
}


- (void) reinit:(int) baseTime; {
    NSArray *steps;
    int n = baseTime;
    if ([self.tableName isEqualToString:@"SqrA"]) {
        steps   =  @[
                     @[@(n), @0,     @(2*n), @0],
                     @[@(n), @(n),   @(2*n), @0],
                     @[@(n), @(n),   @(2*n), @(n)],
                     @[@(n), @(2*n), @(2*n), @(n)],
                     @[@(n), @(2*n), @(2*n), @(2*n)],
                     ];
    } else if ([self.tableName isEqualToString:@"SqrB"]) {
        steps   =  @[
                     @[@(n)  , @(n)  , @(n*2)    , @(n  )],
                     @[@(n+1), @(n+1), @((n+1)*2), @(n+1)],
                     @[@(n+2), @(n+2), @((n+2)*2), @(n+2)],
                     @[@(n+3), @(n+3), @((n+3)*2), @(n+3)],
                     @[@(n+4), @(n+4), @((n+4)*2), @(n+4)],
                     ];
    } else if ([self.tableName isEqualToString:@"SqrC"]) {
        steps   =  @[
                     @[@(n), @((n+0)*2), @(n*2), @(n)],
                     @[@(n), @((n+1)*2), @(n*2), @(n+1)],
                     @[@(n), @((n+2)*2), @(n*2), @(n+2)],
                     @[@(n), @((n+3)*2), @(n*2), @(n+3)],
                     @[@(n), @((n+4)*2), @(n*2), @(n+4)],
                     ];
    }
    self.stepsInitial = (NSMutableArray*)steps;
    self.timeBase     = baseTime;
    [self reset];
}


@end
