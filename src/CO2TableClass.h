//
//  CO2TableClass.h
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableEx.h"

@interface CO2TableClass : TrainingTable
-(CO2TableClass*) initWithTimeBase:(int) baseTime;
@end
