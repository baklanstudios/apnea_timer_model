//
//  TrainingTable.m
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTable.h"
#import "Tools.h"

@interface TrainingTable()

@end

@implementation TrainingTable

@synthesize currentStep;
@synthesize currentPhase;
@synthesize totalTime;

#pragma mark - Initialization

- (TrainingTable*) initWithPhases:(NSMutableArray*) phases_ andPhasesAbbr:(NSArray*) phasesAbbr_ andSteps:(NSMutableArray*)  steps_ andTableName:(NSString*) tableName_ andTimeBase:(int) timeBase_ {
    self = [super init];
    self.stepsInitial = steps_;
    self.phases       = phases_;
    self.phasesAbbr   = phasesAbbr_;
    self.tableName    = tableName_;
    self.timeBase     = timeBase_;
    [self reset];
    return self;
}

- (void) reset {
    self.steps        = [self copyArray:self.stepsInitial];
    self.totalTime    = [self getTotalTime];
    self.currentStep  =  0;
    self.currentPhase = -1;
    
    if (self.delegate) {
        [self.delegate updatePhaseTime];
        [self.delegate updateTotalTime];
        [self.delegate setActivePhase];
        [self.delegate setNextPhase];
    }

}

- (void) reinit:(int) baseTime; {
    mustOverride(); // or methodNotImplemented(), same thing
}
    
#pragma mark - Training Table Operations

- (int) nextStep {
    self.currentStep++;
    if (self.currentStep < self.steps.count){
        self.currentPhase = 0;
    } else {
        self.currentStep  = -1;
        self.currentPhase = -1;
    }
//    self.totalTime = [self getTotalTime];
    return self.currentStep;
}

- (int) getNextStep {
    if (self.currentStep < self.steps.count){
        return self.currentStep + 1;
    } else {
        return -1;
    }
}

- (int) nextPhase:(BOOL) consumeCycle {
    if (self.currentStep >= 0) {
        if (self.currentPhase >= 0) {
            [self.steps[self.currentStep] replaceObjectAtIndex:self.currentPhase withObject:[NSNumber numberWithFloat:0.0f]];
            self.totalTime = [self getTotalTime];
        }
        self.currentPhase ++;
        if (self.currentPhase >= self.phases.count)
            [self nextStep];
        // to skip zero-duration phases
        if ([self getPhaseDuration:self.currentPhase forStep:self.currentStep] == 0) {
            [self nextPhase:NO];
            return self.currentPhase;
        }

        if (self.delegate && self.currentStep >= 0) {
            [self.delegate setActivePhase];
            [self.delegate setNextPhase];
        }

    }

    return self.currentPhase;
}

#pragma mark - Training Table Details

- (NSString*) phaseDescription {
    return [self getPhaseDescription:self.currentPhase];
}

- (NSString*) phaseAbbreviation {
    return [self getPhaseAbbreviation:self.currentPhase];
}

- (float) phaseDuration {
    return [self getPhaseDuration:self.currentPhase forStep:self.currentStep];
}

- (PhaseId) getNextPhaseIdForStep:(int)step andPhase:(int)phase{
    PhaseId pi;
    pi.step  = step;
    pi.phase = phase + 1;

    if (pi.phase >= self.phases.count) {
        pi.step++;
        pi.phase = 0;
    }
    
    if ([self getPhaseDuration:pi.phase forStep:pi.step] == 0.0f)
        return [self getNextPhaseIdForStep:pi.step andPhase:pi.phase];
    else
        return pi;
}

- (NSString*) getNextPhaseDescription {
    PhaseId pi = [self getNextPhaseIdForStep:self.currentStep andPhase:self.currentPhase];
    if (pi.step >= self.steps.count || pi.step < 0)
        return nil;
    else
        return [self getPhaseDescription:pi.phase];
}

- (float) getNextPhaseDuration {
    PhaseId pi = [self getNextPhaseIdForStep:self.currentStep andPhase:self.currentPhase];
    if (pi.step >= self.steps.count || pi.step < 0)
        return -1;
    else
        return [self getInitialPhaseDuration:pi.phase forStep:pi.step];
}

- (float) getPhaseDuration:(int) phase forStep:(int)step {
    if (phase >=0 && phase < self.phases.count && step >= 0 && step < self.steps.count) {
        NSArray * st = self.steps[step];
        float dur = [((NSNumber*)st[phase]) floatValue];
        return dur;
    } else {
        return -1;
    }
}

- (float) getInitialPhaseDuration:(int) phase forStep:(int)step {
    if (phase >=0 && phase < self.phases.count && step >= 0 && step < self.steps.count) {
        NSArray * st = self.stepsInitial[step];
        float dur = [((NSNumber*)st[phase]) floatValue];
        return dur;
    } else {
        return -1;
    }
}

- (NSString *) getPhaseAbbreviation:(int) phase {
    if (phase >=0 && phase < self.phasesAbbr.count)
        return [self.phasesAbbr objectAtIndex:phase];
    else
        return nil;
}

- (NSString *) getPhaseDescription:(int) phase {
    if (phase >=0 && phase < self.phases.count)
        return [self.phases objectAtIndex:phase];
    else
        return nil;
}

#pragma mark - Training Operation

- (BOOL) timeStep {
    return [self timeStep:1.0f];
}

- (BOOL) timeStep:(float) step {
    if (self.currentStep >= 0) {
        if (self.currentPhase >=0 &&
            self.currentPhase < self.phases.count &&
            [self phaseDuration] > 0) {
            [self consumeCurrentPhaseTime:step];
        } else {
            if ([self nextPhase:YES] >= 0) {
                if ([self phaseDuration] > 0) {
                    [self consumeCurrentPhaseTime:step];
                } else {
                    if ([self nextPhase:NO] >= 0)
                        [self consumeCurrentPhaseTime:step];
                }
            }
        }
    }
    if (self.delegate && self.currentStep >= 0) {
        [self.delegate updatePhaseTime];
        [self.delegate updateTotalTime];
    }
    return NO;
}

- (void) consumeCurrentPhaseTime:(float) step {
    NSMutableArray * st = self.steps[self.currentStep];
    float dur = [(NSNumber*)st[self.currentPhase] floatValue];
    float newDur = dur - step;
    
    if (newDur < 0) newDur = 0;
    NSNumber *newVal = [NSNumber numberWithFloat:newDur];
    [st replaceObjectAtIndex:self.currentPhase withObject:newVal];
    self.totalTime -= step;
}

#pragma mark - Utility Functions

- (NSString*) getName {
    return self.tableName;
}

- (float) getTotalTime {
    float result = 0.0f;
    for (NSArray *arr in self.steps) {
        for (NSNumber * num in arr) {
            result += [num floatValue];
        }
    }
    return result;
}

- (NSMutableArray*) copyArray:(NSArray*) array {
    NSMutableArray *copy = [[NSMutableArray alloc] init];
    for (NSArray *arr in array) {
        NSMutableArray *marr = [[NSMutableArray alloc] init];
        for (NSNumber *number in arr) {
            NSNumber *val = [[NSNumber alloc] initWithFloat:[number floatValue]];
            [marr addObject:val];
        }
        [copy addObject:marr];
    }
    return copy;
}

- (void) printArrays:(BOOL) withInitial {
     if (withInitial) {
         printf("Initial Array:\n");
         for (NSArray *arr in self.stepsInitial) {
             for (NSNumber *val in arr) {
                 printf("%.2f  ", val.floatValue);
             }
             printf("\n");
         }
     }
     printf("Working Array:\n");
     for (NSArray *arr in self.steps) {
         for (NSNumber *val in arr) {
             printf("%.2f  ", val.floatValue);
         }
         printf("\n");
     }
 }

@end