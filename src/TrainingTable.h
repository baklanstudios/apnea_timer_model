//
//  TrainingTable.h
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct
{
    int step;
    int phase;
} PhaseId;

@protocol TimerViewProtocol

- (void) updateTotalTime;
- (void) updatePhaseTime;
- (void) setActivePhase;
- (void) setNextPhase;
- (void) playPhaseSound;

@end

@interface TrainingTable : NSObject

#pragma mark - Properties
@property (strong, nonatomic) NSArray        *phases;
@property (strong, nonatomic) NSArray        *phasesAbbr;
@property (strong, nonatomic) NSArray        *stepsInitial;
@property (strong, nonatomic) NSMutableArray *steps;
@property (strong, nonatomic) NSString       *tableName;
@property int currentStep;
@property int currentPhase;
@property float totalTime;
@property (weak, nonatomic) id<TimerViewProtocol> delegate;
@property int timeBase;

#pragma mark - Initialization

- (TrainingTable*) initWithPhases:(NSArray*) phases_ andPhasesAbbr:(NSArray*) phasesAbbr_ andSteps:(NSArray*) steps_ andTableName:(NSString*) tableName_ andTimeBase:(int) timeBase_;

- (void)      reset;
- (void)      reinit:(int) baseTime;

#pragma mark - Table Navigation
- (int)       nextStep;
- (int)       getNextStep;
- (int)       nextPhase:(BOOL) consumeCycles;
//- (int)       nextPhase;

#pragma mark - Phase Information
- (NSString*) phaseAbbreviation;
- (NSString*) phaseDescription;
- (float)     phaseDuration;
- (NSString*) getNextPhaseDescription;
- (float)     getNextPhaseDuration;
- (NSString*) getPhaseDescription:(int) phase;
- (NSString*) getPhaseAbbreviation:(int) phase;
- (float)     getPhaseDuration:(int) phase forStep:(int)step;
- (float)     getInitialPhaseDuration:(int) phase forStep:(int)step;
- (float)     getTotalTime;

- (PhaseId) getNextPhaseIdForStep:(int)step andPhase:(int)phase;
- (NSString*) getName;

#pragma mark - Operation
- (BOOL)      timeStep;
- (BOOL)      timeStep:(float) step;

#pragma mark - Utility Functions
- (void)      printArrays:(BOOL) withInitial;

@end
