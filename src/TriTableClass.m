//
//  TriTableClass.m
//  Apnea Timer
//
//  Created by Mikhail on 16.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TriTableClass.h"

@implementation TriTableClass

-(TriTableClass*) initTableA:(NSString*) tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_ {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"exhale", nil)
                       ];
    
    NSArray *abbr   = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"exhale", nil)
                       ];
    
    int n = timeBase_;
    NSArray *steps   =  @[
                          @[@(n),   @(n*2)],
                          @[@(n+1), @((n+1)*2)],
                          @[@(n+2), @((n+2)*2)],
                          @[@(n+3), @((n+3)*2)],
                          @[@(n+4), @((n+4)*2)],
                          @[@(n+5), @((n+5)*2)],
                          ];
    
    NSArray *cycles = @[@(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_)];
    
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps andCycles:cycles andTableName:tableName_ andTimeBase:timeBase_];
    
    self.repeats = repeats_;
    //NSLog(@"%@ - repeats: %d", self.tableName, [self.cycles[0] intValue]);
    
    return self;
}

-(TriTableClass*) initTableB:(NSString*) tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_ {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"exhale", nil)
                       ];
    NSArray *abbr   = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"exhale", nil)
                       ];
    int n = timeBase_;
    NSArray *steps   =  @[
                          @[@(n   ) , @( n   * 2)], // 4 x 8
                          @[@(n+ 2) , @((n+ 2)*2)], // 6 x 12
                          @[@(n+ 4) , @((n+ 4)*2)], // 8 x 16
                          @[@(n+ 6) , @((n+ 6)*2)], // 10 x 20
                          @[@(n+ 8) , @((n+ 8)*2)], // 12 x 24
                          @[@(n+10) , @((n+10)*2)], // 14 x 28
                         ];
    
    NSArray *cycles = @[@(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_), @(repeats_)];
    
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps andCycles:cycles andTableName:tableName_ andTimeBase:timeBase_];
    
    self.repeats = repeats_;

    return self;
}

- (void) reinitWithRepeats:(int) repeats; {
    NSArray *cycles;
    if ([self.tableName isEqualToString:@"TriA"]) {
        cycles = @[@(repeats), @(repeats), @(repeats), @(repeats), @(repeats), @(repeats)];
    }  else if ([self.tableName isEqualToString:@"TriB"]) {
        cycles = @[@(repeats), @(repeats), @(repeats), @(repeats), @(repeats), @(repeats)];
    }
    self.cyclesInitial = (NSMutableArray*)cycles;
    self.repeats       = repeats;
    [self reset];
}

- (void) reinit:(int) baseTime; {
    NSArray *steps;
    int n = baseTime;
    if ([self.tableName isEqualToString:@"TriA"]) {
        steps   =  @[
                     @[@(n),   @(n*2)],
                     @[@(n+1), @((n+1)*2)],
                     @[@(n+2), @((n+2)*2)],
                     @[@(n+3), @((n+3)*2)],
                     @[@(n+4), @((n+4)*2)],
                     @[@(n+5), @((n+5)*2)],
                     ];
    } else if ([self.tableName isEqualToString:@"TriB"]) {
        steps   =  @[
                     @[@(n   ) , @( n   * 2)], // 4 x 8
                     @[@(n+ 2) , @((n+ 2)*2)], // 6 x 12
                     @[@(n+ 4) , @((n+ 4)*2)], // 8 x 16
                     @[@(n+ 6) , @((n+ 6)*2)], // 10 x 20
                     @[@(n+ 8) , @((n+ 8)*2)], // 12 x 24
                     @[@(n+10) , @((n+10)*2)], // 14 x 28
                     ];
    }
    self.stepsInitial = (NSMutableArray*)steps;
    self.timeBase     = baseTime;
    [self reset];
}

@end
