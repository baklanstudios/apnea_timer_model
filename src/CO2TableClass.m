//
//  CO2TableClass.m
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "CO2TableClass.h"

@implementation CO2TableClass

#pragma mark -

- (CO2TableClass *) init {
    return [self initNormal];
//    return [self init1Phase];
//    return [self init2Phases];
//    return [self init3Phases];
//    return [self init4Phases];
}

-(CO2TableClass*) initNormal {
    return [self initWithTimeBase:10];
}

-(CO2TableClass*) initWithTimeBase:(int)baseTime {
    NSArray *phases = @[
                        NSLocalizedString(@"ventilate", nil),
                        NSLocalizedString(@"hold", nil)
                        ];
    NSArray *abbr   = phases;
    NSArray *steps  = @[
                        @[@(baseTime*12), @(baseTime*12)], // 1
                        @[@(baseTime*12), @(baseTime*12)], // 1
                        @[@(baseTime*11), @(baseTime*12)], // 1
                        @[@(baseTime*10), @(baseTime*12)], // 1
                        @[@(baseTime*9 ), @(baseTime*12)], // 1
                        @[@(baseTime*8 ), @(baseTime*12)], // 1
                        @[@(baseTime*7 ), @(baseTime*12)], // 1
                        @[@(baseTime*6 ), @(baseTime*12)]  // 1
                        ];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"CO2" andTimeBase: baseTime];
    return self;
}

- (void) reinit:(int) baseTime; {
    NSArray *steps  = @[
                        @[@(baseTime*12), @(baseTime*12)], // 1
                        @[@(baseTime*12), @(baseTime*12)], // 1
                        @[@(baseTime*11), @(baseTime*12)], // 1
                        @[@(baseTime*10), @(baseTime*12)], // 1
                        @[@(baseTime*9 ), @(baseTime*12)], // 1
                        @[@(baseTime*8 ), @(baseTime*12)], // 1
                        @[@(baseTime*7 ), @(baseTime*12)], // 1
                        @[@(baseTime*6 ), @(baseTime*12)]  // 1
                        ];
    self.stepsInitial = (NSMutableArray*)steps;
    self.timeBase     = baseTime;
    [self reset];
}

#pragma => Testing Tables

-(CO2TableClass*) init4Phases {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"full lungs apnea", nil),
                        NSLocalizedString(@"exhale", nil),
                        NSLocalizedString(@"empty lungs apnea", nil)
                        ];
    NSArray *abbr   = @[
                        @"IN",
                        @"APFL",
                        @"EX",
                        @"APEL"];
    NSArray *steps  = @[
                        @[@5.0, @5.0, @10, @0],
                        @[@5.0, @5.0, @10, @5],
                        //                        @[@5.0, @5.0, @10, @10]
                        ];
//    NSArray *cycles = @[@1, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"CO2" andTimeBase:10];
    return self;
}

-(CO2TableClass*) init3Phases {
    NSArray *phases = @[
                        NSLocalizedString(@"inhale", nil),
                        NSLocalizedString(@"full lungs apnea", nil),
                        NSLocalizedString(@"exhale", nil),
                       ];
    NSArray *abbr   = @[@"IN", @"APFL", @"EX"];
    NSArray *steps  = @[
                        @[@1.0, @0.0, @3.0],
                        @[@2.0, @0.0, @4.0],
                       ];
//    NSArray *cycles = @[@2, @2];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"CO2" andTimeBase:10];
    return self;
}

-(CO2TableClass*) init2Phases {
    NSArray *phases = @[@"inhale", @"exhale"];
    NSArray *abbr   = @[@"inhale", @"exhale"];
    NSArray *steps  = @[
                        @[@5.0, @3.0],
                        @[@5.0, @4.0],
                        @[@5.0, @5.0]

                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        ,@[@5.0, @5.0]
                        
                        ];
//    NSArray *cycles = @[@2, @2, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"CO2" andTimeBase:10];
    return self;
}

-(CO2TableClass*) init1Phase {
    NSArray *phases = @[@"inhale"];
    NSArray *abbr   = @[@"inhale"];
    NSArray *steps  = @[
                        @[@5.0],
                        @[@5.0],
                        @[@5.0]
                        ];
    //    NSArray *cycles = @[@2, @2, @1];
    self = [super initWithPhases:phases andPhasesAbbr:abbr andSteps:steps /*andCycles:cycles*/ andTableName:@"CO2" andTimeBase:10];
    return self;
}

@end
