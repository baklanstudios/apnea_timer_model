//
//  SqTableClass.h
//  Apnea Timer
//
//  Created by Mikhail on 16.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableEx.h"

@interface SqTableClass : TrainingTableEx

-(SqTableClass*) initTableA:(NSString*)tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_;
-(SqTableClass*) initTableB:(NSString*)tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_;
-(SqTableClass*) initTableC:(NSString*)tableName_ withTimeBase:(int)timeBase_ andRepeat:(int)repeats_;

@end
