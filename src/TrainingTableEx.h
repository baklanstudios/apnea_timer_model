//
//  TrainingTableEx.h
//  Apnea Timer
//
//  Created by Mikhail on 07.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

/**
 **   Extension of TrainingTable class with added support for
 **   number of cycles for each training step
 **/

#import "TrainingTable.h"

@interface TrainingTableEx : TrainingTable

#pragma mark - Properties

@property (strong, nonatomic) NSArray *cyclesInitial;
@property (strong, nonatomic) NSMutableArray *cycles;

@property int repeats;

#pragma mark - Initialization

- (TrainingTableEx*) initWithPhases:(NSArray*) phases_ andPhasesAbbr:(NSArray*) phasesAbbr_ andSteps:(NSArray*) steps_ andCycles:(NSArray*) cycles_ andTableName:(NSString*) tableName_ andTimeBase:(int)timeBase_;

- (void)      reinitWithRepeats:(int) repeats;

@end


@interface TrainingTableEx (Protected)

#pragma mark - Table Information
- (NSString*)       getPhaseDescription:(int)      phase;
- (float)           getPhaseDuration:   (int)      phase   forStep:(int)step;
- (int)             getCycleRepeats;

#pragma mark - Utility Functions
- (NSMutableArray*) copyArray:          (NSArray*) array;

@end